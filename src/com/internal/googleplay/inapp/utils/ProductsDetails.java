/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.internal.googleplay.inapp.utils;

import java.util.HashMap;
import java.util.TreeMap;

/**
 * Represents a block of information about in-app items.
 */
public class ProductsDetails extends TreeMap<String, Product> {

    /** Returns the listing details for an in-app product. */
    public Product getDetails(String sku) {
        return get(sku);
    }

    /** Return whether or not details about the given product are available. */
    public boolean hasDetails(String sku) {
        return containsKey(sku);
    }

    void addProductDetails(Product d) {
        put(d.getSku(), d);
    }

    public String getPrice(String sku){
        return get(sku).getPrice();
    }
}
