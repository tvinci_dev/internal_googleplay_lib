package com.internal.googleplay.inapp;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.internal.googleplay.inapp.utils.IabHelper;
import com.internal.googleplay.inapp.utils.IabResult;
import com.internal.googleplay.inapp.utils.Purchase;

import java.util.ArrayList;

/**
 * Created by tehilarozin on 9/21/15.
 */
public class InappService {
    static final String TAG = "TrivialDrive";

    static final String ACTIVITY_RESULT = "inner.activity.result";
    static final String ACTIVITY_CREATE = "inner.activity.create";

    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;
    public static final String PRODUCT_CODE = "productCode";
    public static final String PRODUCT_TYPE = "productType";
    public static final String PAYLOAD = "payload";


    // The helper object
    IabHelper mHelper;

    boolean mInappActive;
    private InnerActivity mInnerActivity;
    private String mPurchasePayload = "";

    private static InappService sInstance;
    private IabHelper.OnIabPurchaseFinishedListener mPurchaseListener;

    public static InappService self(){
        if(sInstance == null || (sInstance != null && sInstance.mHelper == null)){
            throw new RuntimeException("InappService should be initialized first");
        }
        return sInstance;
    }

    public static InappService init(Context context, String publicKey){
        if(sInstance == null){
            sInstance = new InappService(context, publicKey);
        }

        return sInstance;
    }

    private InappService(Context context, String publicKey){

        //String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlY4nTLGki7ClJsJiN3eTn84p7ZqS+IPgMBK8GWsxMSUEHXBKuURBimYXv5aV5o7eKTP5HF02+Qn24cRc4DVjRseBnMMt+uxusTCw3BSR0wr7MTctFjGJgtxfi+5HMPOLo3jxGpyH42scOTt9X1B0BTr8qscSlYLdFscOwwFtd0YY8oowUsIhsqnwGhBi+/VPNfDWuRBj9wZj8yvEP6qDlysXbtPig20LzNmoXzIkCJGmYY5uqzhNdBmoE4fBFiJSnEJviRYPl/MISS9VB0hNRvAiBNo27WM4PMKV7p0FSpjBJcHEJos++nB8JQo4sc4/+TSp4TneD/29N4VaRXp4gwIDAQAB";
        IntentFilter filter = new IntentFilter("inner.activity.create");
        filter.addAction("inner.activity.result");

        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");

        //GooglePlayApp.self().registerActivityLifecycleCallbacks(this);

        mHelper = new IabHelper(context, publicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);
       // mInnerActivity =   new InnerActivity().;
        activateIab();
    }

    private void activateIab() {
        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                mInappActive = result.isSuccess();

                if (mInappActive) {
                    Log.d(TAG, "Setup finished.");

                } else {
                    Log.e(TAG, "Failed to setup Google billing connection");
                }
            }
        });
    }

    /**
     * Async query for products details from Google play service
     * @param productType can be ITEM_TYPE_SUBS or ITEM_TYPE_INAPP {@link com.internal.googleplay.inapp.utils.InAppConstants}
     * @param skus list of products ids (codes) to fetch their details
     * @param listener will handle the query results
     */
    public void getProductsDetails(String productType, ArrayList<String> skus, IabHelper.ProductsQueryListener listener){
        if (mHelper == null) return;
        mHelper.queryProductsDetailsAsync(productType, skus, listener);
    }

    /**
     * activates purchase of product in Google play and returns result containing the purchase token
     * if purchase succeeded.
     */
    public void purchase(String payload, String productType, String productCode, IabHelper.OnIabPurchaseFinishedListener listener) {
        mPurchasePayload = payload;
        mPurchaseListener = listener;

        Log.d(TAG, "Launching purchase flow for infinite gas subscription.");

        Bundle data = new Bundle();
        data.putString(PAYLOAD, payload);
        data.putString(PRODUCT_TYPE, productType);
        data.putString(PRODUCT_CODE, productCode);

        Intent startIntent  = new Intent();
        startIntent.setAction("internal_action_dummy");
        startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startIntent.putExtras(data);
        mHelper.getContext().startActivity(startIntent);
    }

    public static ActivityLifecycleCallbacks getActivityLifeCycleListener() {
        return self().mLifecycleCallbacks;
    }


    ActivityLifecycleCallbacks mLifecycleCallbacks = new ActivityLifecycleCallbacks() {

        @Override
        public void handleActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            if (mHelper != null) {
                mHelper.handleActivityResult(activity, requestCode, resultCode, data);
            }
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            Bundle data = activity.getIntent().getExtras();
            if (data != null) {
                data.setClassLoader(activity.getClassLoader());
                mHelper.launchPurchaseFlow(activity, data.getString(PRODUCT_CODE), data.getString(PRODUCT_TYPE), RC_REQUEST, mPurchaseListener, data.getString(PAYLOAD));
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    };





    // Called when consumption is complete
    /*IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();

            }
            else {
                complain("Error while consuming: " + result);
            }
            updateUi();
            setWaitScreen(false);
            Log.d(TAG, "End consumption flow.");
        }
    };*/

    // We're being destroyed. It's important to dispose of the helper here!
    public void onDestroy() {
        mInnerActivity.finish();

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    /*public void onActivityStarted(Activity activity, Bundle bundle) {
        if(bundle!=null) {
            mPurchasePayload = bundle.getString("payload");
            mHelper.launchPurchaseFlow(activity, bundle.getString("productId"), bundle.getString("productType"), RC_REQUEST, mPurchaseListener, mPurchasePayload);
        }

    }*/

    public interface ActivityLifecycleCallbacks extends Application.ActivityLifecycleCallbacks{
        void handleActivityResult(Activity activity, int requestCode, int resultCode, Intent data);
    }
}
