package com.internal.googleplay.inapp;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by tehilarozin on 11/1/15.
 */
public class InnerActivity extends Activity {

    private InappService.ActivityLifecycleCallbacks mLifecycleCallbacks;

    public InnerActivity(){
        super();

        mLifecycleCallbacks = InappService.getActivityLifeCycleListener();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(mLifecycleCallbacks != null) {
            mLifecycleCallbacks.handleActivityResult(this, requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(mLifecycleCallbacks != null) {
            mLifecycleCallbacks.onActivityCreated(this, savedInstanceState);
        }
    }
}
