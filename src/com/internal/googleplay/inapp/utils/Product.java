/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.internal.googleplay.inapp.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents an in-app product's listing details.
 */
public class Product {
    private String mProductType;
    private String mSku;
    private String mType;
    private String mPrice;
    private double mPriceValue;
    private String mCurrencyCode;
    private String mTitle;
    private String mDescription;
    private String mJson;

    public Product(String productType, String jsonSkuDetails) throws JSONException {
        mProductType = productType;
        mJson = jsonSkuDetails;
        JSONObject o = new JSONObject(mJson);
        mSku = o.optString(InAppConstants.PRODUCT_ID);
        mType = o.optString(InAppConstants.TYPE);
        mPrice = o.optString(InAppConstants.PRICE);
        mPriceValue = o.optDouble(InAppConstants.PRICE_VALUE) / 1000000;
        mCurrencyCode = o.optString(InAppConstants.CURRENCY_CODE);
        mTitle = o.optString(InAppConstants.TITLE);
        mDescription = o.optString(InAppConstants.DESCRIPTION);
    }

    public String getSku() { return mSku; }
    public String getProductType() { return mProductType; }
    public String getType() { return mType; }
    public String getPrice() { return mPrice; }
    public String getTitle() { return mTitle; }
    public String getDescription() { return mDescription; }

    public String getCurrencyCode() {
        return mCurrencyCode;
    }

    public double getPriceValue() {
        return mPriceValue;
    }

    public String log() {
        return "Product:" + mJson;
    }
}
