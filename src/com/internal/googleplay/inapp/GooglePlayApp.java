package com.internal.googleplay.inapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

/**
 * Created by tehilarozin on 9/21/15.
 */
public class GooglePlayApp extends Application {

    private static GooglePlayApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
    }

    public static GooglePlayApp self(){
        return sInstance;
    }

    public static Context getContext(){return sInstance;}



}
